SHELL := /bin/bash

all: set services

services: start_speedtest_service start_speedtestdatabase_service start_nginx_service

start_nginx_service: set_nginx start_speedtest_service
	sudo systemctl start nginx

start_speedtestdatabase_service: set
	sudo ln -s /var/www/html/speedtest-project/utils/systemd/speedtestdatabase.service /etc/systemd/system/speedtestdatabase.service
	sudo systemctl start speedtestdatabase.service
	sudo systemctl enable speedtestdatabase.service

start_speedtest_service: set
	sudo ln -s /var/www/html/speedtest-project/utils/systemd/speedtest.service /etc/systemd/system/speedtest.service
	sudo systemctl start speedtest.service
	sudo systemctl enable speedtest.service

set_nginx: 
	sudo cp utils/nginx/speedtest /etc/nginx/sites-available/
	sudo ln -s /etc/nginx/sites-available/speedtest /etc/nginx/sites-enabled/speedtest

set: create_venv install_dependencies create_table

create_table: install_dependencies
	aqui/bin/python utils/create_table.py

install_dependencies: create_venv
	aqui/bin/pip install -r requirements.txt

create_venv: 
	python3 -m venv aqui

#mv_workdir: 
#	sudo mv ../speedtest-project /var/www/html/

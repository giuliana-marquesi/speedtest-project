import time
import sqlite3
import speedtest

st = speedtest.Speedtest()
mantem_loop = True

while mantem_loop:
    conn = sqlite3.connect('speedtest.db')
    cursor = conn.cursor()
    download = st.download()
    upload = st.upload()
    date = time.time()
    if download and upload and date:
        if not cursor.execute("""INSERT INTO speedtest (date, download, upload) VALUES (?,?,?)""", (date, download, upload)):
            mantem_loop = False
    else:
        mantem_loop = False
    conn.commit()
    conn.close()
    time.sleep(1800)


import sqlite3
conn = sqlite3.connect('speedtest.db')
cursor = conn.cursor()

print("creating speedtest table...")
print("date column with REAL type, not null and PRIMARY KEY")
print("download column with REAL type, not null")
print("update column with REAL type not null")

cursor.execute("""
    CREATE TABLE speedtest (
    date REAL NOT NULL PRIMARY KEY,
    download REAL NOT NULL,
    upload REAL NOT NULL
    );
""")

print("Table successfully created!")

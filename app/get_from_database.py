import sqlite3


def conv_B_to_MB(input_bytes):
	convert_mb = input_bytes * 0.000001
	return convert_mb


def get_from_database():


	downloads = []
	uploads = []
	date = []

	conn = sqlite3.connect('speedtest.db')
	cursor = conn.cursor()

	for row in cursor.execute("SELECT * FROM speedtest ORDER BY date DESC LIMIT 48"):
		
		down_MBs = conv_B_to_MB(row[1])
		up_MBs = conv_B_to_MB(row[2])
		date.append(row[0])
		downloads.append(down_MBs)
		uploads.append(up_MBs)

	conn.close()

	return date, downloads, uploads
from app.get_from_database import get_from_database as gfdb
from app.do_graph import do_graph as dg
from flask import Flask
from flask import render_template

app = Flask(__name__)
 
@app.route("/")
def hello():
	date, download, upload = gfdb()
	download_image = dg('Speedtest Download', download, date)
	upload_image = dg('Speedtest Upload', upload, date)

	return render_template('hello.html', download_image=download_image, upload_image=upload_image)

if __name__ == "__main__":
	app.run(host="0.0.0.0")

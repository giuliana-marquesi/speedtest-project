from matplotlib import pyplot as plt
from matplotlib import dates as mtdates
from matplotlib import ticker as plticker
from pytz import timezone
from io import BytesIO
import base64


def do_graph(title, data, date):

	buf = BytesIO()

	date_fmt = '%d-%m-%y %H:%M:%S'
	hour = mtdates.HourLocator()
	new_dates = mtdates.epoch2num(date)
	formatter = mtdates.DateFormatter(date_fmt)

	plt.rcParams['figure.figsize'] = (11,7)
	formatter.set_tzinfo(timezone('America/Sao_Paulo'))
	
	plt.plot(new_dates,data)

	plt.gca().xaxis.set_major_locator(hour)
	plt.gca().yaxis.set_major_locator(plticker.MultipleLocator(5))

	plt.gca().xaxis.set_major_formatter(formatter)
	

	plt.gcf().autofmt_xdate()

	plt.title(title)
	plt.ylabel('MB/s')
	plt.savefig(buf,format='png')
	data = base64.b64encode(buf.getbuffer()).decode("ascii")
	plt.close()

	return data
